#![feature(backtrace)]

#[macro_use]
extern crate dlopen_derive;

#[macro_use]
extern crate thiserror;

use {
    dlopen::wrapper::{Container, WrapperApi},
    libc::c_char,
    std::{ffi::CStr, path::Path, backtrace::Backtrace},
};

#[derive(Error, Debug)]
pub enum BindError {
    #[error("Could not interface with Raildriver.dll / Raildriver64.dll: {}", source)]
    Dlopen {
        #[from]
        source: dlopen::Error,
        backtrace: Backtrace,
    },
    #[error("Train Simulator is not connected. Have you started a scenario yet?")]
    TrainSimNotConnected,
}

#[derive(WrapperApi)]
struct RaildriverDll {
    #[dlopen_name = "SetRailDriverConnected"]
    set_rail_driver_connected: extern "C" fn(connect: bool),

    #[dlopen_name = "SetRailSimConnected"]
    get_rail_sim_connected: extern "C" fn() -> bool,

    #[dlopen_name = "GetControllerList"]
    get_controller_list: unsafe extern "C" fn() -> *const c_char,

    #[dlopen_name = "GetCurrentControllerValue"]
    get_current_controller_value: extern "C" fn(controller_index: i32) -> f32,

    #[dlopen_name = "SetControllerValue"]
    set_controller_value: extern "C" fn(controller_index: i32, new_value: f32),
}

pub struct TrainSimBindings {
    container: Container<RaildriverDll>,
}

impl TrainSimBindings {
    pub fn new(path: &Path) -> Result<Self, BindError> {
        let container: Container<RaildriverDll> = unsafe { Container::load(path) }?;

        let connection = Self { container };
        connection.set_rail_driver_connected(true);

        if connection.controllers().is_empty() {
            return Err(BindError::TrainSimNotConnected);
        }

        Ok(connection)
    }

    pub fn controllers(&self) -> Vec<String> {
        let controllers: Vec<String>;

        unsafe {
            let raw_ptr = self.container.get_controller_list();
            let unsafe_controllers = CStr::from_ptr(raw_ptr);
            let unsafe_controllers = unsafe_controllers.to_string_lossy();
            controllers = unsafe_controllers
                .split("::")
                .filter(|controller| controller != &"")
                .map(String::from)
                .collect();
            // NOTE: This leaks memory. Since the Railworks DLL does not provide access to its `free` implementation,
            //       I cannot deallocate `raw_ptr`. This will remain the case unless I find a nasty hack/workaround.
        }

        controllers
    }

    pub fn get_current_controller_value(&self, controller_index: i32) -> f32 {
        self.container
            .get_current_controller_value(controller_index)
    }

    fn set_rail_driver_connected(&self, flag: bool) {
        self.container.set_rail_driver_connected(flag);
    }

    pub fn set_current_controller_value(&self, controller_index: i32, new_value: f32) {
        self.container.set_controller_value(controller_index, new_value);
    }
}
