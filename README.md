# ts-controller-tools

A collection of more or less useful tools for controlling or otherwise interacting with
Dovetail Games' Train Simulator (not Train Sim World) series.

## Features
### Cruise Control

Do you wish you could just sit in the passenger view of your train and enjoy the world
go past while someone else does the driving for you? Then you'll like
`cruise-control`.

All you have to do is set up the train ready for departure, open your browser on any device in
your local network (e.g. your phone) and tell it about the current and upcoming speed limits
and stops.

### "Sven's Controller" (experimental)

Support for a particular type of controller designed for the Japanese BVE simulator.
Over time, this is intended to grow into generic support for joysticks and similar controllers.

## Installation
### Windows Binary Release

Currently, binary releases are only provided for the 64-bit version of Train Simulator.
These will not work with 32-bit Train Simulator.

- Download the most recent release from [GitLab](https://gitlab.com/AndreasKarg/ts-controller-tools/-/releases).
- Extract the zip file to a new folder of your choice.
- Create a new folder `<Your Documents folder>\ts-controller-tools`
- Copy `config-template.toml` from the zip file into the new folder and rename it to `config.toml`
- Open `config.toml` in your favourite text editor (Notepad will do) and change the first line
  to your Train Simulator installation path.
- Open a command-line terminal in the folder you extracted `ts-controller-tools` into.
- Run `ts-controller-tools.exe print-controllers`.
- If all went well, it should now print
  `Error: Train Simulator is not connected. Have you started a scenario yet?` and quit - or,
  if you are already running a Train Simulator scenario, print a long list of things.

### From Source

---
**NOTE**

`ts-controller-tools` currently requires a Nightly build of the Rust toolchain.

---
- Clone the repository
- Copy `config-template.toml` from the zip file into the new folder and rename it to `config.toml`
- Open `config.toml` in your favourite text editor (Notepad will do) and change the first line
  to your Train Simulator installation path.
- Run `cargo run -- print-controllers` from the repo root folder.
- If all went well, it should now print
  `Error: Train Simulator is not connected. Have you started a scenario yet?` and quit - or,
  if you are already running a Train Simulator scenario, print a long list of things.

## Usage

- Open a command-line terminal in the installation folder.
- Run `ts-controller-tools.exe <command> [parameters]` to start the tool or
  `ts-controller-tools.exe --help` for more information.

License: Apache-2.0
