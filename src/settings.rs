use std::collections::HashMap;
use {
    crate::CRATE_NAME,
    config::{Config, ConfigError, File},
    directories::UserDirs,
    std::path::{Path, PathBuf},
};

#[derive(Debug, Error)]
pub enum ValidationError {
    #[error("Could not find Raildriver DLL at {:?}", path)]
    DllNotFound { path: PathBuf },
}

#[derive(Debug, Error)]
pub enum SettingsError {
    #[error("Failed to load the config file: {}", source)]
    ConfigFile {
        #[from]
        source: ConfigError,
    },
    #[error("The config file failed validation: {}", source)]
    ValidationError {
        #[from]
        source: ValidationError,
    },
    #[error("Failed to find the current user's document dir.")]
    DocumentDirNotFound,
}

#[derive(Debug, Deserialize, Serialize, Copy, Clone)]
pub struct MinMax {
    pub min: f64,
    pub max: f64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ControllerIds {
    pub throttle: String,
    pub brake: String,
    pub speedometer: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct VehicleProfile {
    pub throttle_range: MinMax,
    pub brake_range: MinMax,
    pub controller_ids: ControllerIds,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Settings {
    trainsim_path: Box<Path>,
    pub vehicle_profiles: HashMap<String, VehicleProfile>,
}

impl Settings {
    pub fn load() -> Result<Self, SettingsError> {
        let config_file_base_path = UserDirs::new()
            .ok_or(SettingsError::DocumentDirNotFound)?
            .document_dir()
            .ok_or(SettingsError::DocumentDirNotFound)?
            .join(PathBuf::from(CRATE_NAME))
            .join(PathBuf::from("config"));
        let mut s = Config::new();

        s.merge(File::from(config_file_base_path))?;

        let settings: Settings = s.try_into()?;

        settings.validate()?;

        Ok(settings)
    }

    #[cfg(target_arch = "x86")]
    const RAILDRIVER_DLL_PATH: &'static str = r"plugins\RailDriver.dll";

    #[cfg(target_arch = "x86_64")]
    const RAILDRIVER_DLL_PATH: &'static str = r"plugins\RailDriver64.dll";

    pub fn raildriver_dll_path(&self) -> PathBuf {
        self.trainsim_path
            .join(PathBuf::from(Self::RAILDRIVER_DLL_PATH))
    }

    fn validate(&self) -> Result<(), ValidationError> {
        let dll_path = self.raildriver_dll_path();
        if !dll_path.exists() {
            Err(ValidationError::DllNotFound { path: dll_path })
        } else {
            Ok(())
        }
    }
}
