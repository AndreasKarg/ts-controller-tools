use tokio::sync::{broadcast, mpsc};

pub trait FastForward<T> {
    type Err;

    fn try_get_latest_value(&mut self) -> Result<Option<T>, Self::Err>;
}

impl<T: Clone> FastForward<T> for broadcast::Receiver<T> {
    type Err = broadcast::error::TryRecvError;

    fn try_get_latest_value(&mut self) -> Result<Option<T>, Self::Err> {
        let mut entry = None;
        loop {
            match self.try_recv() {
                Err(Self::Err::Lagged(_)) => {
                    continue;
                }
                Ok(new_entry) => {
                    entry = Some(new_entry);
                }
                Err(Self::Err::Empty) => {
                    break;
                }
                Err(other) => {
                    return Err(other);
                }
            }
        }

        Ok(entry)
    }
}

impl<T: Clone> FastForward<T> for mpsc::Receiver<T> {
    type Err = mpsc::error::TryRecvError;

    fn try_get_latest_value(&mut self) -> Result<Option<T>, Self::Err> {
        let mut entry = None;
        loop {
            match self.try_recv() {
                Ok(new_entry) => {
                    entry = Some(new_entry);
                }
                Err(Self::Err::Empty) => {
                    break;
                }
                Err(other) => {
                    return Err(other);
                }
            }
        }

        Ok(entry)
    }
}
