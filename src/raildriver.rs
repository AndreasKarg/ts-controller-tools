use {
    super::settings::ControllerIds,
    std::{convert::TryInto, path::Path},
    tokio::{
        sync::broadcast::{error::SendError, Sender},
        time::{interval, sleep, Duration},
    },
    trainsim_bindings::{BindError, TrainSimBindings},
};

#[derive(Clone, Debug)]
pub struct Sample {
    pub speed: f32,
    pub throttle: f32,
    pub brake: f32,
}

#[derive(Error, Debug)]
pub enum RaildriverError {
    #[error("Failed to bind to DLL: {source}")]
    BindError {
        #[from]
        source: BindError,
    },
    #[error("Could not find a controller named \"{0}\".")]
    ControllerNotFound(String),
    #[error(
        "Something strange has happened. Somehow the index of controller \"{name}\" \
             is outside the range it can realistically ever be. If you ever encounter this error, \
             please report it to the author! Details: {source}"
    )]
    TryFromIntError {
        name: String,
        source: std::num::TryFromIntError,
    },
}

pub struct Raildriver {
    speed_index: i32,
    throttle_index: i32,
    brake_index: i32,
    connection: TrainSimBindings,
}

impl Raildriver {
    pub async fn new(
        raildriver_dll_path: &Path,
        controller_ids: &ControllerIds,
    ) -> Result<Self, RaildriverError> {
        let connection = loop {
            // TODO: Reintroduce logging here
            match TrainSimBindings::new(raildriver_dll_path) {
                Ok(connection) => {
                    break connection;
                }
                Err(BindError::TrainSimNotConnected) => {}
                Err(other) => {
                    return Err(other.into());
                }
            }
            sleep(Duration::from_millis(1000)).await;
        };

        let controllers = connection.controllers();

        let speed_index =
            Raildriver::find_controller_index(&controllers, &controller_ids.speedometer)?;
        let throttle_index =
            Raildriver::find_controller_index(&controllers, &controller_ids.throttle)?;
        let brake_index = Raildriver::find_controller_index(&controllers, &controller_ids.brake)?;

        Ok(Raildriver {
            speed_index,
            throttle_index,
            brake_index,
            connection,
        })
    }

    fn find_controller_index(controllers: &[String], name: &str) -> Result<i32, RaildriverError> {
        controllers
            .iter()
            .position(|entry| entry == name)
            .ok_or_else(|| RaildriverError::ControllerNotFound(name.to_owned()))?
            .try_into()
            .map_err(|err| RaildriverError::TryFromIntError {
                name: name.to_owned(),
                source: err,
            })
    }

    pub async fn run_raildriver(
        &self,
        sample_tx: Sender<Sample>,
        sample_interval: Duration,
    ) -> Result<(), SendError<Sample>> {
        let connection = &self.connection;
        let mut interval_timer = interval(sample_interval);

        loop {
            interval_timer.tick().await;

            let speed = connection.get_current_controller_value(self.speed_index);
            let throttle = connection.get_current_controller_value(self.throttle_index);
            let brake = connection.get_current_controller_value(self.brake_index);
            let sample = Sample {
                speed,
                throttle,
                brake,
            };

            sample_tx.send(sample)?;
        }
    }

    pub fn set_throttle(&self, new_value: f32) {
        self.connection
            .set_current_controller_value(self.throttle_index, new_value)
    }

    pub fn set_brake(&self, new_value: f32) {
        self.connection
            .set_current_controller_value(self.brake_index, new_value)
    }
}
