#[cfg(any(test, feature = "dummy-multiinput"))]
use dummy_multiinput as selected_multiinput;

#[cfg(not(any(test, feature = "dummy-multiinput")))]
use multiinput as selected_multiinput;

use {
    self::selected_multiinput::{DeviceType, RawEvent, RawInputManager, State, XInputInclude, Axis},
    super::{
        raildriver::{Raildriver, RaildriverError},
        settings::MinMax,
        settings::Settings,
    },
    num_enum::TryFromPrimitive,
    std::{collections::HashMap, convert::TryFrom},
    tokio::time::{interval, Duration},
};

#[derive(Error, Debug)]
pub enum InputDeviceError {
    #[error("Could not enumerate input devices: {0}")]
    RawInputManager(String),
    #[error("Joystick state was not available.")]
    NoJoystickState,
    #[error("Could not connect to Train Simulator: {source}")]
    RaildriverError {
        #[from]
        source: RaildriverError,
    },
}

pub async fn main(
    settings: Settings,
    vehicle_profile: String,
    poll_interval: Duration,
    debounce_threshold: u16,
) -> Result<(), InputDeviceError> {
    let vehicle_profile = &settings.vehicle_profiles[&vehicle_profile];
    let mut manager =
        RawInputManager::new().map_err(|err| InputDeviceError::RawInputManager(err.to_owned()))?;
    manager.register_devices(DeviceType::Joysticks(XInputInclude::True));

    let devices = manager.get_device_list();

    manager.filter_devices(
        devices
            .joysticks
            .iter()
            .map(|device| device.name.clone())
            .collect(),
    );

    println!(
        "Here are the devices I found:\n{:#?}\n\n",
        devices.joysticks
    );

    let stick_state = manager
        .get_joystick_state(0)
        .ok_or(InputDeviceError::NoJoystickState)?;

    println!(
        "Here are the details of the first joystick:\n{:#?}\n\n",
        stick_state
    );

    let raildriver = Raildriver::new(
        settings.raildriver_dll_path().as_path(),
        &vehicle_profile.controller_ids,
    )
    .await?;

    gamepad_pump(
        &mut manager,
        raildriver,
        vehicle_profile.throttle_range,
        vehicle_profile.brake_range,
        poll_interval,
        debounce_threshold,
    )
    .await
}

async fn gamepad_pump(
    manager: &mut RawInputManager,
    raildriver: Raildriver,
    throttle_range: MinMax,
    brake_range: MinMax,
    poll_interval: Duration,
    debounce_threshold: u16,
) -> Result<(), InputDeviceError> {

    let mut interval = interval(poll_interval);


    let joystick_state = manager
        .get_joystick_state(0)
        .ok_or(InputDeviceError::NoJoystickState)?;
    let mut button_states: Vec<bool> = joystick_state
        .button_states
        .clone();

    let mut x_axis = 0.0;
    let mut y_axis = 0.0;
    let mut z_axis = 0.0;

    let mut button_debounce_count = 0;

    loop {
        interval.tick().await;
        for event in manager.get_events() {
            match event {
                RawEvent::JoystickButtonEvent(0, button, state) => {
                    button_states[button] = match state {
                        State::Pressed => true,
                        State::Released => false,
                    };
                }
                RawEvent::JoystickAxisEvent(0, axis, new_value) => {
                    match axis {
                        Axis::X => { x_axis = new_value; },
                        Axis::Y => { y_axis = new_value; },
                        Axis::Z => { z_axis = new_value; },
                        _ => {}
                    }
                },
                _ => {}
            }
        }

        println!("X: {}\n\tY: {}\n\t\tZ: {}\n", x_axis, y_axis, z_axis);

        let z_min = -0.92;
        let z_max = -0.52;

        let y_min = 0.07;
        let y_max = -0.59;

        let z_axis = (z_axis - z_min) / (z_max - z_min);
        let y_axis = (y_axis - y_min) / (y_max - y_min);

        let brake = y_axis * brake_range.max;
        let throttle = z_axis * throttle_range.max;

        raildriver.set_brake(brake as f32);
        raildriver.set_throttle(throttle as f32);
    }
}

#[cfg(any(test, feature = "dummy-multiinput"))]
mod dummy_multiinput {
    pub enum DeviceType {
        Joysticks(XInputInclude),
    }
    pub enum RawEvent {
        JoystickButtonEvent(usize, usize, State),
    }
    pub enum State {
        Pressed,
        Released,
    }
    pub enum XInputInclude {
        True,
    }
    pub struct RawInputManager {}

    pub struct DevicesDisplayInfo {
        pub joysticks: Vec<JoystickDisplayInfo>,
    }

    #[derive(Debug)]
    pub struct JoystickState {
        pub button_states: Vec<bool>,
    }

    #[derive(Debug)]
    pub struct JoystickDisplayInfo {
        pub name: String,
    }

    impl RawInputManager {
        pub fn new() -> Result<Self, &'static str> {
            panic!()
        }

        pub fn register_devices(&self, _: DeviceType) {
            panic!()
        }

        pub fn get_device_list(&self) -> DevicesDisplayInfo {
            panic!()
        }

        pub fn filter_devices(&self, _: Vec<String>) {
            panic!()
        }

        pub fn get_joystick_state(&self, _: usize) -> Option<JoystickState> {
            panic!()
        }

        pub fn get_events(&mut self) -> std::sync::mpsc::TryIter<RawEvent> {
            panic!()
        }
    }
}
