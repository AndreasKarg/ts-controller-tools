use {
    super::{num_helpers::FractionalScalar, vehicle_parameters::VehicleParameters},
    uom::si::{f64::*, ratio::ratio},
};

#[cfg(test)]
mod simple_acceleration_controller_tests {
    use {
        super::{super::vehicle_parameters, *},
        float_cmp::ApproxEqUlps,
        uom::si::{acceleration::meter_per_second_squared, velocity::mile_per_hour},
    };

    #[test]
    fn throttle_value_is_proportional_to_requested_acceleration_at_given_speed() {
        let vehicle = vehicle_parameters::class_156_params();
        let controller = SimpleAccelerationController {
            vehicle_params: &vehicle,
        };

        for some_speed in [0.0, 5.7, 47.5, 75.0].iter() {
            let some_speed = Velocity::new::<mile_per_hour>(*some_speed);
            let max_te = vehicle.max_te_at(some_speed);
            let max_acceleration = max_te / vehicle.mass;

            for acceleration_factor in [0.0, 0.45, 1.0].iter() {
                let requested_acceleration = *acceleration_factor * max_acceleration;
                let throttle_value =
                    *controller.calculate_throttle_setpoint(requested_acceleration, some_speed);

                assert!(
                    throttle_value.approx_eq_ulps(acceleration_factor, 2),
                    "Throttle did not match for requested acceleration {:.2} at speed {:.1}.",
                    requested_acceleration.get::<meter_per_second_squared>(),
                    some_speed.get::<mile_per_hour>()
                );
            }
        }
    }
}

pub struct SimpleAccelerationController<'a> {
    pub vehicle_params: &'a VehicleParameters,
}

impl SimpleAccelerationController<'_> {
    pub fn calculate_throttle_setpoint(
        &self,
        requested_acceleration: Acceleration,
        speed: Velocity,
    ) -> FractionalScalar {
        let required_te: Force = requested_acceleration * self.vehicle_params.mass;
        let required_throttle: Ratio = required_te / self.vehicle_params.max_te_at(speed);
        FractionalScalar::clamped_new(required_throttle.get::<ratio>())
    }
}
