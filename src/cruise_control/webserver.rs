use {
    super::{num_helpers::NonNegative, ControlCommand, SpeedLimiterUpdate},
    anyhow::Result,
    std::sync::{Arc, Mutex},
    tokio::sync::mpsc::Sender,
    uom::si::{
        acceleration::meter_per_second_squared, f64::*, length::mile, velocity::mile_per_hour,
    },
    warp::{reject::Rejection, Filter, Reply},
};

async fn set_speed(
    speed: i32,
    mut control_command_tx: Sender<ControlCommand>,
) -> Result<impl Reply, Rejection> {
    let speed = Velocity::new::<mile_per_hour>(speed as f64);
    let command = ControlCommand::SetSpeed(speed);
    send_command(&mut control_command_tx, command).await
}

async fn speed_at_distance(
    speed: i32,
    distance: f32,
    mut control_command_tx: Sender<ControlCommand>,
) -> Result<impl Reply, Rejection> {
    let speed = Velocity::new::<mile_per_hour>(speed as f64);
    let distance = Length::new::<mile>(distance as f64);
    let acceleration =
        NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(0.5)).unwrap();

    let command = ControlCommand::ReachSpeedAtDistance(speed, distance, acceleration);
    send_command(&mut control_command_tx, command).await
}

async fn get_status(update: Arc<Mutex<Option<SpeedLimiterUpdate>>>) -> Result<String, Rejection> {
    let update = update.lock().unwrap();

    if let Some(update) = &*update {
        let response = format!("Current Speed: {:.1} mph\nThrottle: {:.0} %\nBrake: {:.0} %\nHard Limit: {:.1} mph\nEco Limit: {:.1} mph\nLimiter State:\n{}", update.sample.speed, update.sample.throttle * 100.0, update.sample.brake * 100.0, update.effective_limit.hard_limit.get::<mile_per_hour>(), update.effective_limit.eco_limit.get::<mile_per_hour>(), update.speed_limit_merger_debug);
        Ok(response)
    } else {
        Ok(String::from("None yet."))
    }
}

async fn decelerate(
    mut control_command_tx: Sender<ControlCommand>,
) -> Result<impl Reply, Rejection> {
    let command = ControlCommand::Decelerate;
    send_command(&mut control_command_tx, command).await
}

pub async fn run(
    control_command_tx: Sender<ControlCommand>,
    speed_limiter_updates: Arc<Mutex<Option<SpeedLimiterUpdate>>>,
) -> Result<(), std::convert::Infallible> {
    let another_control_command_tx = control_command_tx.clone();
    let yet_another_control_command_tx = control_command_tx.clone();
    let set_speed = warp::path!("set_speed" / i32)
        .and(warp::any().map(move || control_command_tx.clone()))
        .and_then(set_speed);

    let decelerate = warp::path!("decelerate")
        .and(warp::any().map(move || another_control_command_tx.clone()))
        .and_then(decelerate);

    let speed_at_distance = warp::path!("speed_at_distance" / i32 / f32)
        .and(warp::any().map(move || yet_another_control_command_tx.clone()))
        .and_then(speed_at_distance);

    let get_status = warp::path!("get_status")
        .and(warp::any().map(move || speed_limiter_updates.clone()))
        .and_then(get_status);

    let root = warp::path::end().and(warp::fs::file("www/index.html"));

    let binds = set_speed
        .or(decelerate)
        .or(speed_at_distance)
        .or(get_status)
        .or(root);

    warp::serve(binds).bind(([0, 0, 0, 0], 8000)).await;

    Ok(())
}

async fn send_command(
    control_command_tx: &mut Sender<ControlCommand>,
    command: ControlCommand,
) -> Result<impl Reply, Rejection> {
    match control_command_tx.send(command).await {
        Ok(()) => Ok(warp::reply::reply()),
        Err(_) => Err(warp::reject()),
    }
}
