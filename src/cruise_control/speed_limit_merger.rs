use uom::si::f64::*;

#[cfg(test)]
mod tests {
    use {
        super::*,
        uom::si::{length::meter, velocity::mile_per_hour},
    };

    #[derive(Debug)]
    enum StubSpeedLimitState {
        Expired,
        Active(SpeedLimit),
        Preemptible(SpeedLimit),
    }

    #[derive(Debug)]
    struct StubSpeedLimit {
        pub state: StubSpeedLimitState,
    }

    impl DynamicSpeedLimit for StubSpeedLimit {
        fn update(
            self: Box<Self>,
            _distance_travelled_since_last_update: Length,
            _delta_t: Time,
        ) -> SpeedLimitState {
            match self.state {
                StubSpeedLimitState::Expired => SpeedLimitState::Expired,
                StubSpeedLimitState::Active(_) => SpeedLimitState::Active(self),
                StubSpeedLimitState::Preemptible(_) => SpeedLimitState::Preemptible(self),
            }
        }

        fn get_limit(&self) -> SpeedLimit {
            match self.state {
                StubSpeedLimitState::Expired => {
                    unreachable!()
                }
                StubSpeedLimitState::Active(limit) => limit,
                StubSpeedLimitState::Preemptible(limit) => limit,
            }
        }
    }

    impl StubSpeedLimit {
        pub fn new_lowest_active(limit: SpeedLimit) -> Box<StubSpeedLimit> {
            Box::new(Self {
                state: StubSpeedLimitState::Active(limit),
            })
        }

        pub fn new_lowest_preemptible(limit: SpeedLimit) -> Box<StubSpeedLimit> {
            Box::new(Self {
                state: StubSpeedLimitState::Preemptible(limit),
            })
        }

        pub fn new_expired() -> Box<StubSpeedLimit> {
            Box::new(Self {
                state: StubSpeedLimitState::Expired,
            })
        }
    }

    #[derive(Debug)]
    struct ExpiringActiveSpeedLimit {
        pub expiration_countdown: i32,
        pub limit: SpeedLimit,
    }

    impl DynamicSpeedLimit for ExpiringActiveSpeedLimit {
        fn update(
            mut self: Box<Self>,
            _distance_travelled_since_last_update: Length,
            _delta_t: Time,
        ) -> SpeedLimitState {
            self.expiration_countdown -= 1;
            if self.expiration_countdown < 0 {
                SpeedLimitState::Expired
            } else {
                SpeedLimitState::Active(self)
            }
        }

        fn get_limit(&self) -> SpeedLimit {
            self.limit
        }
    }

    #[rstest]
    #[test]
    fn if_no_limits_are_defined_returns_none(
        speed_limit_merger: SpeedLimitMerger,
        distance_travelled_since_last_update: Length,
    ) {
        assert_eq!(None, speed_limit_merger.effective_limit());
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());
        assert_eq!(None, speed_limit_merger.effective_limit());
    }

    #[rstest]
    #[test]
    fn a_single_active_speed_limit_is_effective(
        mut speed_limit_merger: SpeedLimitMerger,
        distance_travelled_since_last_update: Length,
        some_speed_limit: SpeedLimit,
    ) {
        let speed_limit = StubSpeedLimit::new_lowest_active(some_speed_limit);
        speed_limit_merger.append(speed_limit);
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            some_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );
    }

    #[rstest]
    #[test]
    fn with_multiple_active_speed_limits_the_lowest_is_effective(
        mut speed_limit_merger: SpeedLimitMerger,
        distance_travelled_since_last_update: Length,
        low_speed_limit: SpeedLimit,
        medium_speed_limit: SpeedLimit,
        high_speed_limit: SpeedLimit,
    ) {
        speed_limit_merger.append(StubSpeedLimit::new_lowest_active(medium_speed_limit));
        speed_limit_merger.append(StubSpeedLimit::new_lowest_active(low_speed_limit));
        speed_limit_merger.append(StubSpeedLimit::new_lowest_active(high_speed_limit));
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            low_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );

        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            low_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );
    }

    #[rstest]
    #[test]
    fn expired_speed_limits_are_discarded(
        mut speed_limit_merger: SpeedLimitMerger,
        distance_travelled_since_last_update: Length,
        some_speed_limit: SpeedLimit,
    ) {
        speed_limit_merger.append(StubSpeedLimit::new_expired());
        speed_limit_merger.append(StubSpeedLimit::new_lowest_active(some_speed_limit));
        speed_limit_merger.append(StubSpeedLimit::new_expired());
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            some_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );
    }

    #[rstest]
    #[test]
    fn a_single_preemptible_limit_is_effective_until_there_is_an_active_one(
        mut speed_limit_merger: SpeedLimitMerger,
        distance_travelled_since_last_update: Length,
        low_speed_limit: SpeedLimit,
        very_high_speed_limit: SpeedLimit,
    ) {
        let preemptible_speed_limit = low_speed_limit;
        let active_speed_limit = very_high_speed_limit;

        speed_limit_merger.append(StubSpeedLimit::new_expired());
        speed_limit_merger.append(StubSpeedLimit::new_lowest_preemptible(
            preemptible_speed_limit,
        ));
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            preemptible_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );

        let mut speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            preemptible_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );

        speed_limit_merger.append(StubSpeedLimit::new_lowest_active(active_speed_limit));
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            active_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );
    }

    #[rstest]
    #[test]
    fn of_multiple_preemptible_limits_the_lowest_is_effective_until_there_is_an_active_one(
        mut speed_limit_merger: SpeedLimitMerger,
        distance_travelled_since_last_update: Length,
        low_speed_limit: SpeedLimit,
        medium_speed_limit: SpeedLimit,
        high_speed_limit: SpeedLimit,
        very_high_speed_limit: SpeedLimit,
    ) {
        speed_limit_merger.append(StubSpeedLimit::new_lowest_preemptible(medium_speed_limit));
        speed_limit_merger.append(StubSpeedLimit::new_lowest_preemptible(low_speed_limit));
        speed_limit_merger.append(StubSpeedLimit::new_lowest_preemptible(high_speed_limit));
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            low_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );

        let mut speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            low_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );

        speed_limit_merger.append(StubSpeedLimit::new_lowest_active(very_high_speed_limit));
        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            very_high_speed_limit,
            speed_limit_merger.effective_limit().unwrap()
        );
    }

    #[rstest]
    #[test]
    fn if_a_preemptible_limit_gets_preempted_by_an_active_limit_and_the_active_one_expires_the_preemptible_one_does_not_come_back(
        mut speed_limit_merger: SpeedLimitMerger,
        distance_travelled_since_last_update: Length,
    ) {
        let preemptible_limit = SpeedLimit {
            hard_limit: Velocity::new::<mile_per_hour>(50.0),
            eco_limit: Velocity::default(),
        };
        let active_limit = SpeedLimit {
            hard_limit: Velocity::new::<mile_per_hour>(25.0),
            eco_limit: Velocity::default(),
        };

        speed_limit_merger.append(StubSpeedLimit::new_lowest_preemptible(preemptible_limit));
        let mut speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());

        assert_eq!(
            preemptible_limit,
            speed_limit_merger.effective_limit().unwrap()
        );

        speed_limit_merger.append(Box::new(ExpiringActiveSpeedLimit {
            limit: active_limit,
            expiration_countdown: 1,
        }));

        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());
        assert_eq!(active_limit, speed_limit_merger.effective_limit().unwrap());

        let speed_limit_merger =
            speed_limit_merger.update(distance_travelled_since_last_update, Time::default());
        assert_eq!(None, speed_limit_merger.effective_limit());
    }

    // TODO: Test eco limit behaviour.
    // TODO: Tidy up added time parameters above

    #[fixture]
    fn speed_limit_merger() -> SpeedLimitMerger {
        SpeedLimitMerger::new()
    }

    #[fixture]
    fn distance_travelled_since_last_update() -> Length {
        Length::new::<meter>(250.0)
    }

    #[fixture]
    fn low_speed() -> Velocity {
        Velocity::new::<mile_per_hour>(25.0)
    }

    #[fixture]
    fn medium_speed() -> Velocity {
        Velocity::new::<mile_per_hour>(40.0)
    }

    #[fixture]
    fn high_speed() -> Velocity {
        Velocity::new::<mile_per_hour>(50.0)
    }

    #[fixture]
    fn very_high_speed() -> Velocity {
        Velocity::new::<mile_per_hour>(75.0)
    }

    #[fixture]
    fn low_speed_limit(low_speed: Velocity) -> SpeedLimit {
        SpeedLimit {
            hard_limit: low_speed,
            eco_limit: Velocity::default(),
        }
    }

    #[fixture]
    fn medium_speed_limit(medium_speed: Velocity) -> SpeedLimit {
        SpeedLimit {
            hard_limit: medium_speed,
            eco_limit: Velocity::default(),
        }
    }

    #[fixture]
    fn high_speed_limit(high_speed: Velocity) -> SpeedLimit {
        SpeedLimit {
            hard_limit: high_speed,
            eco_limit: Velocity::default(),
        }
    }

    #[fixture]
    fn very_high_speed_limit(very_high_speed: Velocity) -> SpeedLimit {
        SpeedLimit {
            hard_limit: very_high_speed,
            eco_limit: Velocity::default(),
        }
    }

    #[fixture]
    fn some_speed_limit(medium_speed: Velocity) -> SpeedLimit {
        SpeedLimit {
            hard_limit: medium_speed,
            eco_limit: Velocity::default(),
        }
    }
}

#[derive(Debug)]
pub struct SpeedLimitMerger {
    limits: Vec<Box<dyn DynamicSpeedLimit>>,
    effective_limit: Option<SpeedLimit>,
}

// TODO: Resolve all the naming overloads - There are too many "SpeedLimit"s

#[derive(Debug, Clone, Copy, PartialEq, new)]
pub struct SpeedLimit {
    pub hard_limit: Velocity,
    pub eco_limit: Velocity,
}

impl SpeedLimit {
    pub fn new_common(limit: Velocity) -> Self {
        Self::new(limit, limit)
    }
}

impl SpeedLimitMerger {
    pub fn new() -> Self {
        Self {
            limits: Vec::new(),
            effective_limit: None,
        }
    }

    pub fn effective_limit(&self) -> Option<SpeedLimit> {
        self.effective_limit
    }

    pub fn update(self, distance_travelled_since_last_update: Length, delta_t: Time) -> Self {
        let new_speed_limit_states: Vec<_> = self
            .limits
            .into_iter()
            .map(|limit| limit.update(distance_travelled_since_last_update, delta_t))
            .collect();

        let (active_limits, preemptible_by_active_limits): (Vec<_>, Vec<_>) =
            new_speed_limit_states
                .into_iter()
                .fold((Vec::new(), Vec::new()), |mut acc, state| {
                    match state {
                        SpeedLimitState::Active(limit) => {
                            acc.0.push(limit);
                        }
                        SpeedLimitState::Preemptible(limit) => {
                            acc.1.push(limit);
                        }
                        SpeedLimitState::Expired => {}
                    };
                    acc
                });

        let effective_speed_limits = if !active_limits.is_empty() {
            active_limits
        } else {
            preemptible_by_active_limits
        };

        let effective_limit = effective_speed_limits
            .iter()
            .map(|limit| limit.get_limit())
            .reduce(|acc, limit| {
                let acc = match limit.hard_limit < acc.hard_limit {
                    true => SpeedLimit {
                        hard_limit: limit.hard_limit,
                        eco_limit: acc.eco_limit,
                    },
                    false => acc,
                };

                match limit.eco_limit < acc.eco_limit {
                    true => SpeedLimit {
                        hard_limit: acc.hard_limit,
                        eco_limit: limit.eco_limit,
                    },
                    false => acc,
                }
            });

        Self {
            limits: effective_speed_limits,
            effective_limit,
        }
    }

    pub fn append(&mut self, limit: Box<dyn DynamicSpeedLimit>) {
        self.limits.push(limit);
    }
}

#[derive(Debug)]
pub enum SpeedLimitState {
    Expired,
    Active(Box<dyn DynamicSpeedLimit>),
    Preemptible(Box<dyn DynamicSpeedLimit>),
}

pub trait DynamicSpeedLimit: std::fmt::Debug + Send {
    fn update(self: Box<Self>, delta_x: Length, delta_t: Time) -> SpeedLimitState;
    fn get_limit(&self) -> SpeedLimit;
}
