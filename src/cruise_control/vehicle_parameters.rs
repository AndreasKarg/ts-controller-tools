use {
    super::num_helpers::{NonNegative, NonNegativeError},
    uom::si::{f64::*, mass::kilogram, time::second, velocity::mile_per_hour},
};

#[cfg(test)]
mod tests {
    use {
        super::*,
        float_cmp::ApproxEqUlps,
        uom::si::{
            acceleration::meter_per_second_squared, force::newton, mass::kilogram, power::watt,
            velocity::meter_per_second,
        },
    };

    #[test]
    fn below_crossover_speed_max_te_at_speed_is_determined_by_overall_max_te() {
        let max_te = Force::new::<newton>(1.0);
        let vehicle_params = VehicleParameters {
            mass: Mass::new::<kilogram>(1.0),
            max_te,
            max_power: Power::new::<watt>(1.0),
        };
        let crossover_speed = Velocity::new::<meter_per_second>(1.0);

        // Sanity check
        assert_eq!(
            crossover_speed,
            vehicle_params.max_power / vehicle_params.max_te
        );

        for speed_factor in [0.0, 0.4, 1.0].iter() {
            assert_eq!(
                max_te,
                vehicle_params.max_te_at(*speed_factor * crossover_speed)
            );
        }
    }

    #[test]
    fn above_crossover_speed_max_te_at_speed_is_determined_by_max_power() {
        let max_te = Force::new::<newton>(1.0);
        let vehicle_params = VehicleParameters {
            mass: Mass::new::<kilogram>(1.0),
            max_te,
            max_power: Power::new::<watt>(1.0),
        };
        let crossover_speed = Velocity::new::<meter_per_second>(1.0);

        // Sanity check
        assert_eq!(
            crossover_speed,
            vehicle_params.max_power / vehicle_params.max_te
        );

        let speed = crossover_speed;
        assert_eq!(max_te, vehicle_params.max_te_at(speed));

        let speed = 2.0 * crossover_speed;
        assert_eq!(max_te / 2.0, vehicle_params.max_te_at(speed));

        let speed = 8.0 * crossover_speed;
        assert_eq!(max_te / 8.0, vehicle_params.max_te_at(speed));
    }

    #[test]
    fn max_acceleration_is_max_te_over_mass() {
        let vehicle = class_156_params();
        let expected_max_acceleration =
            (vehicle.max_te / vehicle.mass).get::<meter_per_second_squared>();
        let actual_max_acceleration = vehicle
            .max_acceleration()
            .unwrap()
            .get::<meter_per_second_squared>();

        assert!(
            expected_max_acceleration.approx_eq_ulps(&actual_max_acceleration, 2),
            "{} is not quite the same as {}, innit?",
            expected_max_acceleration,
            actual_max_acceleration
        );
    }
}

pub struct VehicleParameters {
    pub mass: Mass,
    pub max_te: Force,
    pub max_power: Power,
}

impl VehicleParameters {
    pub fn max_te_at(&self, speed: Velocity) -> Force {
        let theoretical_max_te = self.max_power / speed;
        theoretical_max_te.min(self.max_te)
    }

    pub fn max_acceleration(&self) -> Result<NonNegative<Acceleration>, NonNegativeError> {
        NonNegative::try_new(self.max_te / self.mass)
    }
}

pub fn class_156_params() -> VehicleParameters {
    let t_0_to_10mph = Time::new::<second>(8.0);
    let t_10_to_75_mph = Time::new::<second>(3.0 * 60.0 + 7.55); // 3:07.55
    let t_70_to_75_mph = Time::new::<second>(31.03);

    let vehicle_mass_dms = Mass::new::<kilogram>(35.5e3);
    let vehicle_mass_dmsl = Mass::new::<kilogram>(36.1e3);
    let total_vehicle_mass = vehicle_mass_dms + vehicle_mass_dmsl;

    let ten_mph = Velocity::new::<mile_per_hour>(10.0);
    let seventy_mph = Velocity::new::<mile_per_hour>(70.0);
    let seventy_five_mph = Velocity::new::<mile_per_hour>(75.0);

    let breakaway_acceleration = ten_mph / t_0_to_10mph;
    let breakaway_tractive_effort = breakaway_acceleration * total_vehicle_mass;

    let e_kin_at_10_mph = e_kin(total_vehicle_mass, ten_mph);
    let e_kin_at_70_mph = e_kin(total_vehicle_mass, seventy_mph);
    let e_kin_at_75_mph = e_kin(total_vehicle_mass, seventy_five_mph);

    let delta_e_10_to_75_mph = e_kin_at_75_mph - e_kin_at_10_mph;
    let p_avg_10_to_75_mph = delta_e_10_to_75_mph / t_10_to_75_mph;

    let delta_e_70_to_75_mph = e_kin_at_75_mph - e_kin_at_70_mph;
    let _p_avg_70_to_75_mph = delta_e_70_to_75_mph / t_70_to_75_mph;

    VehicleParameters {
        mass: total_vehicle_mass,
        max_te: breakaway_tractive_effort,
        max_power: p_avg_10_to_75_mph,
    }
}

fn e_kin(m: Mass, v: Velocity) -> Energy {
    0.5 * m * v * v
}
