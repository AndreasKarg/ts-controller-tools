use {
    super::{super::raildriver::Sample, SpeedLimiterUpdate},
    anyhow::{Context, Result},
    itertools::{Itertools, MinMaxResult},
    std::collections::VecDeque,
    tokio::sync::broadcast,
    tui::{
        backend::{Backend, CrosstermBackend},
        layout::{Constraint, Direction, Layout},
        style::{Color, Modifier, Style},
        symbols,
        text::Span,
        widgets::{Axis, Block, Borders, Chart, Dataset, GraphType, List, ListItem},
        Terminal,
    },
    uom::si::{f64::*, velocity::mile_per_hour},
};

unzip_n::unzip_n!(3);

pub struct Tui {
    terminal: Terminal<CrosstermBackend<std::io::Stdout>>,
    samples: VecDeque<Sample>,
    hard_speed_limits: VecDeque<Velocity>,
    eco_speed_limits: VecDeque<Velocity>,
}

// TODO: Tests

impl Tui {
    pub fn new() -> Result<Self> {
        let stdout = std::io::stdout();
        let mut backend = CrosstermBackend::new(stdout);
        backend.clear().context("Error clearing the console.")?;
        let mut terminal = Terminal::new(backend).context("Could not create a new terminal.")?;
        terminal
            .hide_cursor()
            .context("Error whilst hiding the cursor.")?;

        Ok(Tui {
            terminal,
            samples: VecDeque::new(),
            hard_speed_limits: VecDeque::new(),
            eco_speed_limits: VecDeque::new(),
        })
    }

    pub async fn run(
        mut self,
        mut sample_receiver: broadcast::Receiver<SpeedLimiterUpdate>,
    ) -> Result<()> {
        loop {
            let new_sample = sample_receiver.recv().await?;

            if self.samples.len() == 3000 {
                self.samples.pop_front();
                self.hard_speed_limits.pop_front();
                self.eco_speed_limits.pop_front();
            }

            self.samples.push_back(new_sample.sample);
            self.hard_speed_limits
                .push_back(new_sample.effective_limit.hard_limit);
            self.eco_speed_limits
                .push_back(new_sample.effective_limit.eco_limit);

            Tui::render(
                &mut self.terminal,
                &self.samples,
                &self.hard_speed_limits,
                &self.eco_speed_limits,
            )?;
        }
    }

    fn render(
        terminal: &mut Terminal<CrosstermBackend<std::io::Stdout>>,
        samples: &VecDeque<Sample>,
        hard_speed_limits: &VecDeque<Velocity>,
        eco_speed_limits: &VecDeque<Velocity>,
    ) -> Result<(), std::io::Error> {
        terminal.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(1)
                .constraints([Constraint::Percentage(70), Constraint::Percentage(30)].as_ref())
                .split(f.size());

            let (speeds, throttles, brakes) = samples
                .iter()
                .map(|sample| (sample.speed as f64, sample.throttle, sample.brake))
                .unzip_n_vec();

            let hard_speed_limits = extract_numerical_values(hard_speed_limits.iter());
            let eco_speed_limits = extract_numerical_values(eco_speed_limits.iter());

            let all_y_values = speeds
                .iter()
                .chain(hard_speed_limits.iter())
                .chain(eco_speed_limits.iter());
            let (y_min, y_max) = min_max(all_y_values);

            let speeds = enumerate_with_f64(speeds);
            let hard_speed_limits = enumerate_with_f64(hard_speed_limits);
            let eco_speed_limits = enumerate_with_f64(eco_speed_limits);

            let datasets = vec![
                Dataset::default()
                    .name("Speed")
                    .marker(symbols::Marker::Braille)
                    .graph_type(GraphType::Scatter)
                    .style(Style::default().fg(Color::Cyan))
                    .data(speeds.as_slice()),
                Dataset::default()
                    .name("Eco Speed Limit")
                    .marker(symbols::Marker::Braille)
                    .graph_type(GraphType::Line)
                    .style(Style::default().fg(Color::Green))
                    .data(eco_speed_limits.as_slice()),
                Dataset::default()
                    .name("Hard Speed Limit")
                    .marker(symbols::Marker::Braille)
                    .graph_type(GraphType::Line)
                    .style(Style::default().fg(Color::Red))
                    .data(hard_speed_limits.as_slice()),
            ];

            let block = Chart::new(datasets)
                .block(Block::default().title("Live Data").borders(Borders::ALL))
                .style(Style::default().fg(Color::White).bg(Color::Black))
                .x_axis(
                    Axis::default()
                        .title(Span::styled("Sample", Style::default().fg(Color::Red)))
                        .style(Style::default().fg(Color::White))
                        .bounds([0.0, speeds.len() as f64])
                        .labels(
                            [
                                "0s".to_string(),
                                format!("{}s", speeds.len() / 2),
                                format!("{}s", speeds.len()),
                            ]
                            .iter()
                            .cloned()
                            .map(Span::from)
                            .collect(),
                        ),
                )
                .y_axis(
                    Axis::default()
                        .title(Span::styled("MPH", Style::default().fg(Color::Red)))
                        .style(Style::default().fg(Color::White))
                        .bounds([y_min, y_max])
                        .labels(
                            [
                                format!("{:.0}", y_min),
                                format!("{:.0}", y_min + (y_max - y_min) / 4.0),
                                format!("{:.0}", 2.0 * (y_min + (y_max - y_min)) / 4.0),
                                format!("{:.0}", 3.0 * (y_min + (y_max - y_min)) / 4.0),
                                format!("{:.0}", y_max),
                            ]
                            .iter()
                            .cloned()
                            .map(Span::from)
                            .collect(),
                        ),
                );
            f.render_widget(block, chunks[0]);

            let cur_throttle = throttles.last().unwrap();
            let cur_brake = brakes.last().unwrap();

            let list_items = vec![
                ListItem::new(format!("Throttle: >>{}", cur_throttle)),
                ListItem::new(format!("Brake: >>{}", cur_brake)),
            ];

            let block = List::new(list_items)
                .block(Block::default().title("List").borders(Borders::ALL))
                .style(Style::default().fg(Color::White))
                .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
                .highlight_symbol(">>");

            f.render_widget(block, chunks[1]);
        })?;

        Ok(())
    }
}

fn min_max<'a, I: IntoIterator<Item = &'a f64>>(items: I) -> (f64, f64) {
    let min_max_items = items.into_iter().minmax();
    match min_max_items {
        MinMaxResult::MinMax(min, max) => (*min, *max),
        MinMaxResult::NoElements => (0.0, 0.0),
        MinMaxResult::OneElement(y) => (*y, *y),
    }
}

fn extract_numerical_values<'a, I: IntoIterator<Item = &'a Velocity>>(items: I) -> Vec<f64> {
    items
        .into_iter()
        .map(|limit| limit.get::<mile_per_hour>())
        .collect()
}

fn enumerate_with_f64(items: Vec<f64>) -> Vec<(f64, f64)> {
    items
        .into_iter()
        .enumerate()
        .map(|(idx, item)| (idx as f64, item))
        .collect()
}
