use uom::si::f64::*;

#[cfg(test)]
mod tests {
    use {
        super::*,
        uom::si::{
            acceleration::meter_per_second_squared, length::meter, time::second,
            velocity::meter_per_second,
        },
    };

    #[test]
    fn current_max_speed_is_max_safe_speed_in_order_to_reach_target_speed_at_given_deceleration() {
        let speed_over_distance = SpeedOverDistance {};

        let a = Acceleration::new::<meter_per_second_squared>(-0.20);
        let v0 = Velocity::new::<meter_per_second>(30.0);
        let x0 = Length::new::<meter>(-800.0);

        // let v: Velocity = (2.0*a*(x-x0)-(v0*v0)).sqrt();

        let step_size = Time::new::<second>(0.001);

        let mut x = x0;
        for i in 1..100000 {
            let t: Time = (i as f64) * step_size;
            let delta_v: Velocity = a * t;
            let v_actual: Velocity = v0 + delta_v;
            x = x + (delta_v + v0) * step_size;
            let v_calc = speed_over_distance.v(a, x, x0, v0);

            if i % 1000 == 0 {
                println!(
                    "t: {:?}\ndelta_v: {:?}\nx: {:?}\nv_actual: {:?}\nv_calc: {:?}\n\n",
                    t, delta_v, x, v_actual, v_calc
                );
            }

            let v_actual = v_actual.get::<meter_per_second>();
            let v_calc = v_calc.get::<meter_per_second>();

            let v_error = (v_actual - v_calc).abs();

            assert_lt!(
                v_error,
                0.1,
                "{} is too different from {} to be accepted",
                v_calc,
                v_actual
            );
        }
    }
}

pub struct SpeedOverDistance {}

impl SpeedOverDistance {
    // pub fn new(remaining_distance: Length, target_speed: Velocity) -> SpeedOverDistance {
    //     SpeedOverDistance { remaining_distance, target_speed }
    // }

    pub fn v(&self, a: Acceleration, x: Length, x0: Length, v0: Velocity) -> Velocity {
        let v_squared = 2.0 * a * (x - x0) + (v0 * v0);
        let v_squared_signum = match v_squared.is_sign_positive() {
            true => 1.0,
            false => -1.0,
        };
        let v_squared_abs = v_squared.abs();
        let v: Velocity = v_squared_signum * v_squared_abs.sqrt();
        v
        // 2.0 * 1.0 * (10.5 - 0.0) + (10*10)
    }
}
