use uom::si::{f64::*, time::second};

#[cfg(test)]
mod simple_acceleration_controller_tests {
    use {
        super::*,
        float_cmp::ApproxEqRatio,
        uom::si::{acceleration::meter_per_second_squared, velocity::mile_per_hour},
    };

    #[test]
    fn requested_acceleration_is_proportional_to_speed_difference_times_p_factor() {
        let p_factor = 0.1;
        let controller = SimpleSpeedController::new(p_factor);

        let current_speed = Velocity::new::<mile_per_hour>(73.3);
        let setpoint_speed = Velocity::new::<mile_per_hour>(75.0);

        let expected_acceleration_m_per_s_squared = 0.076;

        let actual_acceleration_m_per_s_squared = controller
            .calculate_required_acceleration(current_speed, setpoint_speed)
            .get::<meter_per_second_squared>();

        assert!(
            actual_acceleration_m_per_s_squared
                .approx_eq_ratio(&expected_acceleration_m_per_s_squared, 0.001),
            "{} is too different from {} to be accepted",
            actual_acceleration_m_per_s_squared,
            expected_acceleration_m_per_s_squared
        );
    }
}

pub struct SimpleSpeedController {
    p_factor: f64,
}

impl SimpleSpeedController {
    pub fn new(p_factor: f64) -> SimpleSpeedController {
        SimpleSpeedController { p_factor }
    }

    pub fn calculate_required_acceleration(
        &self,
        current_speed: Velocity,
        setpoint_speed: Velocity,
    ) -> Acceleration {
        let one_second = Time::new::<second>(1.0);
        self.p_factor * (setpoint_speed - current_speed) / one_second
    }
}
