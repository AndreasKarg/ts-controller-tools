use {
    super::num_helpers::NonNegative,
    uom::si::{f64::*, velocity::meter_per_second},
};

#[cfg(test)]
mod tests {
    use {
        super::*,
        float_cmp::ApproxEqUlps,
        uom::si::{
            acceleration::meter_per_second_squared, time::second, velocity::meter_per_second,
        },
    };

    #[test]
    fn setpoint_slides_towards_faster_target_at_requested_acceleration() {
        let interpolator = SetpointInterpolator {};

        let current_setpoint = Velocity::new::<meter_per_second>(10.0);
        let some_target_setpoint = Velocity::new::<meter_per_second>(23.4);
        let acceleration =
            NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(2.5)).unwrap();
        let delta_t = Time::new::<second>(0.3);

        let expected_setpoint_m_per_s = 10.75;
        let actual_setpoint_m_per_s = interpolator
            .slide_to(
                current_setpoint,
                some_target_setpoint,
                acceleration,
                delta_t,
            )
            .get::<meter_per_second>();

        assert!(
            actual_setpoint_m_per_s.approx_eq_ulps(&expected_setpoint_m_per_s, 2),
            "{} is too different from {} to be accepted",
            actual_setpoint_m_per_s,
            expected_setpoint_m_per_s
        );
    }

    #[test]
    fn setpoint_slides_towards_slower_target_at_requested_deceleration() {
        let interpolator = SetpointInterpolator {};

        let current_setpoint = Velocity::new::<meter_per_second>(20.0);
        let some_target_setpoint = Velocity::new::<meter_per_second>(3.2);
        let acceleration =
            NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(2.5)).unwrap();
        let delta_t = Time::new::<second>(0.3);

        let expected_setpoint_m_per_s = 19.25;
        let actual_setpoint_m_per_s = interpolator
            .slide_to(
                current_setpoint,
                some_target_setpoint,
                acceleration,
                delta_t,
            )
            .get::<meter_per_second>();

        assert!(
            actual_setpoint_m_per_s.approx_eq_ulps(&expected_setpoint_m_per_s, 2),
            "{} is too different from {} to be accepted",
            actual_setpoint_m_per_s,
            expected_setpoint_m_per_s
        );
    }

    #[test]
    fn setpoint_does_not_overshoot_target() {
        let interpolator = SetpointInterpolator {};

        let current_setpoint = Velocity::new::<meter_per_second>(20.0);
        let target_setpoint = Velocity::new::<meter_per_second>(21.0);
        let acceleration =
            NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(1.0)).unwrap();
        let delta_t = Time::new::<second>(10.0);

        let expected_setpoint_m_per_s = target_setpoint.get::<meter_per_second>();
        let actual_setpoint_m_per_s = interpolator
            .slide_to(current_setpoint, target_setpoint, acceleration, delta_t)
            .get::<meter_per_second>();

        assert!(
            actual_setpoint_m_per_s.approx_eq_ulps(&expected_setpoint_m_per_s, 2),
            "{} is too different from {} to be accepted",
            actual_setpoint_m_per_s,
            expected_setpoint_m_per_s
        );
    }
}

pub struct SetpointInterpolator {}

impl SetpointInterpolator {
    pub fn slide_to(
        &self,
        current_setpoint: Velocity,
        target_setpoint: Velocity,
        acceleration: NonNegative<Acceleration>,
        delta_t: Time,
    ) -> Velocity {
        let setpoint_error = target_setpoint - current_setpoint;
        let acceleration_signum = setpoint_error.signum().get::<meter_per_second>();

        let delta_v_abs = *acceleration * delta_t;
        let clamped_delta_v_abs = delta_v_abs.min(setpoint_error.abs());
        current_setpoint + clamped_delta_v_abs * acceleration_signum
    }
}
