use {
    num_traits::float::Float,
    uom::{
        num::Num,
        si::{Dimension, Quantity, Units},
        Conversion,
    },
};

#[macro_export]
macro_rules! bounded {
    ($name:ident, $t:ty) => {
        #[derive(Debug)]
        pub struct $name<const LOW: $t, const HIGH: $t>($t);

        impl<const LOW: $t, const HIGH: $t> $name<{ LOW }, { HIGH }> {
            pub const LOW: $t = LOW;
            pub const HIGH: $t = HIGH;

            pub fn clamped_new(n: $t) -> Self {
                $name(n.clamp(Self::LOW, Self::HIGH))
            }

            #[allow(dead_code)]
            pub fn fallible_new(n: $t) -> Result<Self, &'static str> {
                match n {
                    n if n < Self::LOW => Err("Value too low"),
                    n if n > Self::HIGH => Err("Value too high"),
                    n => Ok($name(n)),
                }
            }

            #[allow(dead_code)]
            pub fn clamped_set(&mut self, n: $t) {
                *self = $name(n.clamp(Self::LOW, Self::HIGH))
            }
        }

        impl<const LOW: $t, const HIGH: $t> std::ops::Deref for $name<{ LOW }, { HIGH }> {
            type Target = $t;

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }
    };
}

bounded!(BoundedF64, f64);

pub type FractionalScalar = BoundedF64<0.0, 1.0>;

pub trait IsSignNegative {
    fn is_sign_negative(self) -> bool;
}

impl<D: Dimension + ?Sized, U: Units<V> + ?Sized, V: Conversion<V> + Num + Float> IsSignNegative
    for Quantity<D, U, V>
{
    fn is_sign_negative(self) -> bool {
        self.is_sign_negative()
    }
}

#[derive(Debug)]
pub enum NonNegativeError {
    IsNegativeNumber,
}

#[derive(Debug, Clone)]
pub struct NonNegative<T: IsSignNegative>(T);

impl<T: IsSignNegative + Copy> NonNegative<T> {
    pub fn try_new(val: T) -> Result<NonNegative<T>, NonNegativeError> {
        match val.is_sign_negative() {
            true => Err(NonNegativeError::IsNegativeNumber),
            false => Ok(NonNegative(val)),
        }
    }
}

impl<T: IsSignNegative> std::ops::Deref for NonNegative<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
