use {
    super::{
        num_helpers::NonNegative,
        speed_limit_merger::{DynamicSpeedLimit, SpeedLimit, SpeedLimitState},
        speed_over_distance::SpeedOverDistance,
    },
    uom::si::{acceleration::meter_per_second_squared, f64::*, velocity::mile_per_hour},
};

#[cfg(test)]
mod tests {
    use {
        super::*,
        uom::si::{
            acceleration::meter_per_second_squared,
            length::meter,
            time::second,
            velocity::{meter_per_second, mile_per_hour},
        },
    };

    #[rstest]
    #[test]
    fn a_static_speed_limit_stays_active_for_one_update_then_becomes_preemptible(delta_x: Length) {
        let some_speed = Velocity::new::<mile_per_hour>(17.3);
        let some_speed_limit = SpeedLimit {
            hard_limit: some_speed,
            eco_limit: some_speed,
        };
        let static_speed_limit = Box::new(StaticSpeedLimit::new(some_speed));

        assert_eq!(some_speed_limit, static_speed_limit.get_limit());

        let new_state = static_speed_limit.update(delta_x, Time::default());
        let active_limit = match new_state {
            SpeedLimitState::Active(limit) => limit,
            _ => panic!("Was expecting an active limit!"),
        };

        assert_eq!(some_speed_limit, active_limit.get_limit());

        let new_state = active_limit.update(delta_x, Time::default());
        let preemptible_limit = match new_state {
            SpeedLimitState::Preemptible(limit) => limit,
            _ => panic!("Was expecting a preemptible limit!"),
        };

        assert_eq!(some_speed_limit, preemptible_limit.get_limit());
    }

    #[rstest]
    #[test]
    fn a_braking_curve_speed_limit_keeps_going_down_until_it_reaches_its_setpoint_speed_then_stays_there(
    ) {
        let base_distance = Length::new::<meter>(500.0);
        let target_speed = Velocity::new::<mile_per_hour>(65.0);
        let deceleration =
            NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(0.5)).unwrap();

        let initial_distance = 2.0 * base_distance;

        let braking_curve_speed_limit = Box::new(BrakingCurveSpeedLimit::new(
            initial_distance,
            target_speed,
            deceleration,
        ));

        let initial_speed_limit = braking_curve_speed_limit.get_limit().hard_limit;

        let braking_curve_speed_limit =
            match braking_curve_speed_limit.update(base_distance, Time::default()) {
                SpeedLimitState::Preemptible(limit) => limit,
                _ => panic!("Was expecting a preemptible limit!"),
            };

        let second_speed_limit = braking_curve_speed_limit.get_limit().hard_limit;
        assert_lt!(second_speed_limit, initial_speed_limit);

        let braking_curve_speed_limit =
            match braking_curve_speed_limit.update(base_distance, Time::default()) {
                SpeedLimitState::Preemptible(limit) => limit,
                _ => panic!("Was expecting a preemptible limit!"),
            };

        let third_speed_limit = braking_curve_speed_limit.get_limit().hard_limit;
        assert_eq!(third_speed_limit, target_speed);

        let braking_curve_speed_limit =
            match braking_curve_speed_limit.update(base_distance, Time::default()) {
                SpeedLimitState::Preemptible(limit) => limit,
                _ => panic!("Was expecting a preemptible limit!"),
            };

        let fourth_speed_limit = braking_curve_speed_limit.get_limit().hard_limit;
        assert_eq!(fourth_speed_limit, target_speed);
    }

    // TODO: Test for eco bits as well
    // TODO: Tidy up time parameter in update invocations above.

    #[test]
    fn decelerating_speed_limit_changes_at_constant_deceleration_over_time() {
        let deceleration =
            NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(0.5)).unwrap();
        let initial_speed = Velocity::new::<meter_per_second>(10.0);
        let initial_limit = SpeedLimit::new_common(initial_speed);
        let delta_t = Time::new::<second>(2.5);

        let decelerating_speed_limit =
            Box::new(DeceleratingSpeedLimit::new(deceleration, initial_speed));

        assert_eq!(initial_limit, decelerating_speed_limit.get_limit());

        let decelerating_speed_limit =
            match decelerating_speed_limit.update(Length::default(), delta_t) {
                SpeedLimitState::Preemptible(limit) => limit,
                _ => panic!("Was expecting a preemptible limit!"),
            };

        let expected_speed = initial_speed - Velocity::new::<meter_per_second>(1.25);
        let expected_limit = SpeedLimit::new_common(expected_speed);
        assert_eq!(expected_limit, decelerating_speed_limit.get_limit());

        let decelerating_speed_limit =
            match decelerating_speed_limit.update(Length::default(), delta_t) {
                SpeedLimitState::Preemptible(limit) => limit,
                _ => panic!("Was expecting a preemptible limit!"),
            };

        let expected_speed = expected_speed - Velocity::new::<meter_per_second>(1.25);
        let expected_limit = SpeedLimit::new_common(expected_speed);
        assert_eq!(expected_limit, decelerating_speed_limit.get_limit());
    }

    #[test]
    fn decelerating_speed_limit_bottoms_out_at_secure_stop() {
        let deceleration =
            NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(0.5)).unwrap();
        let initial_speed = Velocity::new::<meter_per_second>(3.0);
        let secure_stop_limit = SpeedLimit::new_common(Velocity::new::<mile_per_hour>(-1.0));
        let delta_t = Time::new::<second>(20.0);

        let decelerating_speed_limit =
            Box::new(DeceleratingSpeedLimit::new(deceleration, initial_speed));

        let decelerating_speed_limit =
            match decelerating_speed_limit.update(Length::default(), delta_t) {
                SpeedLimitState::Preemptible(limit) => limit,
                _ => panic!("Was expecting a preemptible limit!"),
            };
        assert_eq!(secure_stop_limit, decelerating_speed_limit.get_limit());
    }

    #[fixture]
    fn delta_x() -> Length {
        Length::new::<meter>(250.0)
    }
}

#[derive(Debug)]
pub struct StaticSpeedLimit {
    speed: Velocity,
}

impl StaticSpeedLimit {
    pub fn new(speed: Velocity) -> Self {
        Self { speed }
    }
}

impl DynamicSpeedLimit for StaticSpeedLimit {
    fn update(self: Box<Self>, _delta_x: Length, _delta_t: Time) -> SpeedLimitState {
        SpeedLimitState::Active(Box::new(PreemptibleStaticSpeedLimit::new(self.speed)))
    }

    fn get_limit(&self) -> SpeedLimit {
        SpeedLimit {
            hard_limit: self.speed,
            eco_limit: self.speed,
        }
    }
}

#[derive(Debug)]
struct PreemptibleStaticSpeedLimit {
    speed: Velocity,
}

impl PreemptibleStaticSpeedLimit {
    pub fn new(speed: Velocity) -> Self {
        Self { speed }
    }
}

impl DynamicSpeedLimit for PreemptibleStaticSpeedLimit {
    fn update(self: Box<Self>, _delta_x: Length, _delta_t: Time) -> SpeedLimitState {
        SpeedLimitState::Preemptible(self)
    }

    fn get_limit(&self) -> SpeedLimit {
        SpeedLimit {
            hard_limit: self.speed,
            eco_limit: self.speed,
        }
    }
}

#[derive(Debug)]
pub struct BrakingCurveSpeedLimit {
    distance_to_target: Length,
    target_speed: Velocity,
    deceleration: NonNegative<Acceleration>,
}

impl BrakingCurveSpeedLimit {
    pub fn new(
        distance_to_target: Length,
        target_speed: Velocity,
        deceleration: NonNegative<Acceleration>,
    ) -> Self {
        Self {
            distance_to_target,
            target_speed,
            deceleration,
        }
    }
}

impl DynamicSpeedLimit for BrakingCurveSpeedLimit {
    fn update(mut self: Box<Self>, delta_x: Length, _delta_t: Time) -> SpeedLimitState {
        self.distance_to_target -= delta_x;

        if self.distance_to_target > Length::default() {
            SpeedLimitState::Preemptible(self)
        } else {
            SpeedLimitState::Preemptible(Box::new(PreemptibleStaticSpeedLimit::new(
                self.target_speed,
            )))
        }
    }

    fn get_limit(&self) -> SpeedLimit {
        let speed_over_distance = SpeedOverDistance {};
        let origin = Length::default();

        let hard_limit = speed_over_distance.v(
            *self.deceleration,
            self.distance_to_target,
            origin,
            self.target_speed,
        );

        let eco_limit = speed_over_distance.v(
            Acceleration::new::<meter_per_second_squared>(0.1),
            self.distance_to_target,
            origin,
            self.target_speed,
        );

        SpeedLimit {
            hard_limit,
            eco_limit,
        }
    }
}

// TODO: Refactor `update` params with struct

#[derive(Debug, new)]
pub struct DeceleratingSpeedLimit {
    deceleration: NonNegative<Acceleration>,
    initial_speed: Velocity,
    #[new(default)]
    delta_t_cumulative: Time,
}

impl DynamicSpeedLimit for DeceleratingSpeedLimit {
    fn update(mut self: Box<Self>, _delta_x: Length, delta_t: Time) -> SpeedLimitState {
        self.delta_t_cumulative += delta_t;
        let secure_stop_speed = Velocity::new::<mile_per_hour>(-1.0);

        if self.get_speed() <= secure_stop_speed {
            SpeedLimitState::Preemptible(Box::new(PreemptibleStaticSpeedLimit::new(
                secure_stop_speed,
            )))
        } else {
            SpeedLimitState::Preemptible(self)
        }
    }

    fn get_limit(&self) -> SpeedLimit {
        SpeedLimit::new_common(self.get_speed())
    }
}

impl DeceleratingSpeedLimit {
    fn get_speed(&self) -> Velocity {
        self.initial_speed - *self.deceleration * self.delta_t_cumulative
    }
}
