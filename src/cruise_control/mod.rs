mod acceleration_controller;
mod speed_controller;
mod speed_limit_merger;
mod speed_limits;
mod speed_over_distance;
mod vehicle_parameters;
#[macro_use]
mod num_helpers;
mod console_ui;
pub mod webserver;

use {
    super::{
        raildriver::{Raildriver, Sample},
        settings::{MinMax, Settings},
        sync_helpers::FastForward,
    },
    acceleration_controller::SimpleAccelerationController,
    anyhow::{Context, Result},
    console_ui::Tui,
    num_helpers::NonNegative,
    speed_controller::SimpleSpeedController,
    speed_limit_merger::{SpeedLimit, SpeedLimitMerger},
    speed_limits::{BrakingCurveSpeedLimit, DeceleratingSpeedLimit, StaticSpeedLimit},
    std::sync::{Arc, Mutex},
    std::time::Instant,
    tokio::{
        sync::{broadcast, mpsc},
        time::Duration,
        try_join,
    },
    uom::si::{
        acceleration::meter_per_second_squared, f64::*, time::second, velocity::mile_per_hour,
    },
};

const BRAKE_P_FACTOR: f32 = -0.25;

#[derive(Debug, Clone)]
pub enum ControlCommand {
    SetSpeed(Velocity),
    Decelerate,
    ReachSpeedAtDistance(Velocity, Length, NonNegative<Acceleration>),
}

#[derive(Debug, Error)]
pub enum CruiseControlError {
    #[error("All sources of data samples have expired.")]
    NoMoreData,
    #[error("An error in the data sample reception channel has occured: {source}")]
    SampleRecvError {
        #[from]
        source: broadcast::error::RecvError,
    },
    #[error("An error in the cruise control update channel has occured: {inner:?}")]
    UpdateSendError {
        inner: broadcast::error::SendError<SpeedLimiterUpdate>,
    },
    #[error("An error in the sample update channel has occured: {inner:?}")]
    SampleSendError {
        inner: broadcast::error::SendError<Sample>,
    },
    #[error("An error in the data sample reception channel has occured: {source}")]
    CommandRecvError {
        #[from]
        source: mpsc::error::TryRecvError,
    },
}

impl From<broadcast::error::SendError<SpeedLimiterUpdate>> for CruiseControlError {
    fn from(inner: broadcast::error::SendError<SpeedLimiterUpdate>) -> Self {
        Self::UpdateSendError { inner }
    }
}

impl From<broadcast::error::SendError<Sample>> for CruiseControlError {
    fn from(inner: broadcast::error::SendError<Sample>) -> Self {
        Self::SampleSendError { inner }
    }
}

fn tick(
    previous_instant: &mut Instant,
    sample: Sample,
    trainsim: &Raildriver,
    speed_limit_merger: SpeedLimitMerger,
    speed_limiter_update_tx: &mut broadcast::Sender<SpeedLimiterUpdate>,
    throttle_range: MinMax,
    brake_range: MinMax,
) -> Result<SpeedLimitMerger, CruiseControlError> {
    let now = Instant::now();
    let delta_t = now - *previous_instant;
    let delta_t = Time::new::<second>(delta_t.as_secs_f64());
    *previous_instant = now;

    let current_speed = Velocity::new::<mile_per_hour>(sample.speed as f64);
    let vehicle_params = vehicle_parameters::class_156_params();

    let distance_travelled_since_last_update = current_speed * delta_t;
    let speed_limit_merger =
        speed_limit_merger.update(distance_travelled_since_last_update, delta_t);

    let limited_speed = speed_limit_merger.effective_limit().unwrap_or(SpeedLimit {
        hard_limit: Velocity::default(),
        eco_limit: Velocity::default(),
    });

    let speed_controller = SimpleSpeedController::new(0.2);
    let acceleration_controller = SimpleAccelerationController {
        vehicle_params: &vehicle_params,
    };

    let throttle_limit = limited_speed
        .eco_limit
        .max(Velocity::new::<mile_per_hour>(20.0))
        .min(limited_speed.hard_limit);

    let requested_throttle_acceleration =
        speed_controller.calculate_required_acceleration(current_speed, throttle_limit);
    let throttle_setpoint = acceleration_controller
        .calculate_throttle_setpoint(requested_throttle_acceleration, current_speed);
    let throttle_setpoint = *throttle_setpoint * throttle_range.max;

    let speed_difference = (limited_speed.hard_limit.get::<mile_per_hour>() as f32) - sample.speed;
    let brake_setpoint =
        (speed_difference * BRAKE_P_FACTOR).clamp(brake_range.min as f32, brake_range.max as f32);

    trainsim.set_throttle(throttle_setpoint as f32);
    trainsim.set_brake(brake_setpoint);

    let speed_limit_merger_debug = format!("{:#?}", speed_limit_merger);
    let status_update = SpeedLimiterUpdate {
        effective_limit: SpeedLimit {
            hard_limit: limited_speed.hard_limit,
            eco_limit: throttle_limit,
        },
        sample,
        speed_limit_merger_debug,
    };

    speed_limiter_update_tx.send(status_update)?;

    Ok(speed_limit_merger)
}

#[derive(Clone, Debug)]
pub struct SpeedLimiterUpdate {
    pub effective_limit: SpeedLimit,
    pub sample: Sample,
    pub speed_limit_merger_debug: String,
}

pub async fn run_control_loop(
    mut setpoint_receiver: mpsc::Receiver<ControlCommand>,
    mut sample_receiver: broadcast::Receiver<Sample>,
    mut speed_limiter_update_tx: broadcast::Sender<SpeedLimiterUpdate>,
    trainsim: &Raildriver,
    throttle_range: MinMax,
    brake_range: MinMax,
) -> Result<(), CruiseControlError> {
    let mut speed_limit_merger = SpeedLimitMerger::new();

    let requested_speed = loop {
        if let ControlCommand::SetSpeed(speed) = setpoint_receiver
            .recv()
            .await
            .ok_or(CruiseControlError::NoMoreData)?
        {
            break speed;
        }
    };

    speed_limit_merger.append(Box::new(StaticSpeedLimit::new(requested_speed)));

    let mut previous_instant = Instant::now();
    loop {
        let sample = match sample_receiver.recv().await {
            Ok(s) => s,
            Err(broadcast::error::RecvError::Lagged(_)) => sample_receiver.recv().await?,
            Err(other) => return Err(other.into()),
        };

        if let Some(new_command) = setpoint_receiver.try_get_latest_value()? {
            match new_command {
                ControlCommand::SetSpeed(new_setpoint) => {
                    speed_limit_merger.append(Box::new(StaticSpeedLimit::new(new_setpoint)));
                }
                ControlCommand::ReachSpeedAtDistance(speed, distance, deceleration) => {
                    let braking_curve_speed_limit =
                        Box::new(BrakingCurveSpeedLimit::new(distance, speed, deceleration));
                    speed_limit_merger.append(braking_curve_speed_limit);
                }
                ControlCommand::Decelerate => {
                    // TODO: Make this adjustable
                    let deceleration =
                        NonNegative::try_new(Acceleration::new::<meter_per_second_squared>(0.5))
                            .unwrap();
                    let decelerating_speed_limit = Box::new(DeceleratingSpeedLimit::new(
                        deceleration,
                        Velocity::new::<mile_per_hour>(sample.speed as f64),
                    ));
                    speed_limit_merger.append(decelerating_speed_limit);
                }
            }
        }

        speed_limit_merger = tick(
            &mut previous_instant,
            sample,
            trainsim,
            speed_limit_merger,
            &mut speed_limiter_update_tx,
            throttle_range,
            brake_range,
        )?;
    }
}

pub async fn main(settings: Settings, vehicle_profile: String) -> Result<()> {
    let vehicle_profile = settings.vehicle_profiles[&vehicle_profile].clone();

    let (sample_broadcast_tx, cruise_control_sample_rx) = broadcast::channel(16);

    let (setpoint_tx, setpoint_rx) = mpsc::channel(16);

    let (speed_limiter_update_tx, speed_limiter_update_rx) = broadcast::channel(16);
    let speed_limiter_update_state = Arc::new(Mutex::new(None));
    let speed_limiter_update_state_for_pump = speed_limiter_update_state.clone();
    let tui_speed_limiter_update_rx = speed_limiter_update_tx.subscribe();

    let speed_limiter_update_pump = tokio::spawn(async move {
        broadcast_pump(speed_limiter_update_rx, speed_limiter_update_state_for_pump).await
    });

    let webserver =
        tokio::spawn(async move { webserver::run(setpoint_tx, speed_limiter_update_state).await });

    let raildriver = Arc::new(
        Raildriver::new(
            settings.raildriver_dll_path().as_path(),
            &vehicle_profile.controller_ids,
        )
        .await?,
    );
    let another_raildriver = Arc::clone(&raildriver);

    let raildriver_future = tokio::spawn(async move {
        raildriver
            .run_raildriver(sample_broadcast_tx, Duration::from_millis(100))
            .await
    });

    let cruise_control = tokio::spawn(async move {
        run_control_loop(
            setpoint_rx,
            cruise_control_sample_rx,
            speed_limiter_update_tx,
            &another_raildriver,
            vehicle_profile.throttle_range,
            vehicle_profile.brake_range,
        )
        .await
    });

    let tui = tokio::spawn(async move { Tui::new()?.run(tui_speed_limiter_update_rx).await });

    let (
        webserver_result,
        raildriver_result,
        tui_result,
        cruise_control_result,
        speed_limiter_update_pump_result,
    ) = try_join!(
        webserver,
        raildriver_future,
        tui,
        cruise_control,
        speed_limiter_update_pump
    )?;

    webserver_result?;
    raildriver_result.map_err(|err| CruiseControlError::SampleSendError { inner: err })?;
    tui_result?;
    cruise_control_result?;
    speed_limiter_update_pump_result?;

    Ok(())
}

async fn broadcast_pump(
    mut broadcast: broadcast::Receiver<SpeedLimiterUpdate>,
    state: Arc<Mutex<Option<SpeedLimiterUpdate>>>,
) -> Result<()> {
    loop {
        let update = broadcast
            .recv()
            .await
            .context("Something went amiss in the speed limiter update broadcast pump.")?;
        *state.lock().unwrap() = Some(update);
    }
}
