#[cfg(any(test, feature = "dummy-multiinput"))]
use dummy_multiinput as selected_multiinput;

#[cfg(not(any(test, feature = "dummy-multiinput")))]
use multiinput as selected_multiinput;

use {
    self::selected_multiinput::{DeviceType, RawEvent, RawInputManager, State, XInputInclude},
    super::{
        raildriver::{Raildriver, RaildriverError},
        settings::MinMax,
        settings::Settings,
        CRATE_AUTHOR,
    },
    const_format::concatcp,
    num_enum::TryFromPrimitive,
    std::{collections::HashMap, convert::TryFrom},
    tokio::time::{interval, Duration},
};

pub const SUBCOMMAND_AUTHOR: &str = concatcp!(CRATE_AUTHOR, ", Sven Jähnig");

#[derive(Error, Debug)]
pub enum InputDeviceError {
    #[error("Could not enumerate input devices: {0}")]
    RawInputManager(String),
    #[error("Joystick state was not available.")]
    NoJoystickState,
    #[error("Could not connect to Train Simulator: {source}")]
    RaildriverError {
        #[from]
        source: RaildriverError,
    },
}

pub async fn main(
    settings: Settings,
    vehicle_profile: String,
    poll_interval: Duration,
    debounce_threshold: u16,
) -> Result<(), InputDeviceError> {
    let vehicle_profile = &settings.vehicle_profiles[&vehicle_profile];
    let mut manager =
        RawInputManager::new().map_err(|err| InputDeviceError::RawInputManager(err.to_owned()))?;
    manager.register_devices(DeviceType::Joysticks(XInputInclude::True));

    let devices = manager.get_device_list();

    manager.filter_devices(
        devices
            .joysticks
            .iter()
            .map(|device| device.name.clone())
            .collect(),
    );

    println!(
        "Here are the devices I found:\n{:#?}\n\n",
        devices.joysticks
    );

    let stick_state = manager
        .get_joystick_state(0)
        .ok_or(InputDeviceError::NoJoystickState)?;

    println!(
        "Here are the details of the first joystick:\n{:#?}\n\n",
        stick_state
    );

    let raildriver = Raildriver::new(
        settings.raildriver_dll_path().as_path(),
        &vehicle_profile.controller_ids,
    )
    .await?;

    gamepad_pump(
        &mut manager,
        raildriver,
        vehicle_profile.throttle_range,
        vehicle_profile.brake_range,
        poll_interval,
        debounce_threshold,
    )
    .await
}

async fn gamepad_pump(
    manager: &mut RawInputManager,
    raildriver: Raildriver,
    throttle_range: MinMax,
    brake_range: MinMax,
    poll_interval: Duration,
    debounce_threshold: u16,
) -> Result<(), InputDeviceError> {
    let mut controller_hashmap = HashMap::new();
    controller_hashmap.insert(ThrottleLeverState::Emergency, (0.0, 1.0));
    controller_hashmap.insert(ThrottleLeverState::B8, (0.0, 0.75));
    controller_hashmap.insert(ThrottleLeverState::B7, (0.0, 0.75));
    controller_hashmap.insert(ThrottleLeverState::B6, (0.0, 0.75));
    controller_hashmap.insert(ThrottleLeverState::B5, (0.0, 0.5));
    controller_hashmap.insert(ThrottleLeverState::B4, (0.0, 0.5));
    controller_hashmap.insert(ThrottleLeverState::B3, (0.0, 0.25));
    controller_hashmap.insert(ThrottleLeverState::B2, (0.0, 0.25));
    controller_hashmap.insert(ThrottleLeverState::B1, (0.0, 0.25));
    controller_hashmap.insert(ThrottleLeverState::Neutral, (0.0, 0.0));
    controller_hashmap.insert(ThrottleLeverState::P1, (0.25, 0.0));
    controller_hashmap.insert(ThrottleLeverState::P2, (0.5, 0.0));
    controller_hashmap.insert(ThrottleLeverState::P3, (0.5, 0.0));
    controller_hashmap.insert(ThrottleLeverState::P4, (0.75, 0.0));
    controller_hashmap.insert(ThrottleLeverState::P5, (1.0, 0.0));

    let mut interval = interval(poll_interval);

    let mut button_states: Vec<bool> = manager
        .get_joystick_state(0)
        .ok_or(InputDeviceError::NoJoystickState)?
        .button_states
        .clone();
    let mut button_debounce_count = 0;
    let mut candidate_state = ThrottleLeverState::Neutral;
    let mut active_state = ThrottleLeverState::Neutral;

    loop {
        interval.tick().await;
        for event in manager.get_events() {
            if let RawEvent::JoystickButtonEvent(0, button, state) = event {
                button_states[button] = match state {
                    State::Pressed => true,
                    State::Released => false,
                };
            }
        }

        let latest_throttle_state = ThrottleLeverState::try_decode(
            button_states[6],
            button_states[7],
            button_states[8],
            button_states[9],
        );

        if let Some(state) = latest_throttle_state {
            if state == candidate_state {
                button_debounce_count += 1;
            } else {
                candidate_state = state;
                button_debounce_count = 0;
            }
        }

        if ((active_state as i16 - candidate_state as i16).abs() <= 1)
            || (button_debounce_count >= debounce_threshold)
        {
            active_state = candidate_state;
        }

        print!("\x1B[2J\x1B[1;1H");
        println!(
            "Here are the button states of the first joystick:\n{:#?}\n{:?}\n\n",
            button_states, active_state
        );

        let (throttle, brake) = controller_hashmap[&active_state];

        let brake = brake * brake_range.max;
        let throttle = throttle * throttle_range.max;

        raildriver.set_brake(brake as f32);
        raildriver.set_throttle(throttle as f32);
    }
}

#[derive(TryFromPrimitive, Debug, PartialEq, Copy, Clone, Eq, Hash)]
#[repr(u8)]
enum ThrottleLeverState {
    Emergency = 1,
    B8 = 2,
    B7 = 3,
    B6 = 4,
    B5 = 5,
    B4 = 6,
    B3 = 7,
    B2 = 8,
    B1 = 9,
    Neutral = 10,
    P1 = 11,
    P2 = 12,
    P3 = 13,
    P4 = 14,
    P5 = 15,
}

impl ThrottleLeverState {
    pub fn try_decode(b1: bool, b2: bool, b3: bool, b4: bool) -> Option<ThrottleLeverState> {
        let mut numberised: u8 = 0;

        if b1 {
            numberised |= 1 << 3;
        }
        if b2 {
            numberised |= 1 << 2;
        }
        if b3 {
            numberised |= 1 << 1;
        }
        if b4 {
            numberised |= 1 << 0;
        }

        ThrottleLeverState::try_from(numberised).ok()
    }
}

#[cfg(any(test, feature = "dummy-multiinput"))]
mod dummy_multiinput {
    pub enum DeviceType {
        Joysticks(XInputInclude),
    }
    pub enum RawEvent {
        JoystickButtonEvent(usize, usize, State),
    }
    pub enum State {
        Pressed,
        Released,
    }
    pub enum XInputInclude {
        True,
    }
    pub struct RawInputManager {}

    pub struct DevicesDisplayInfo {
        pub joysticks: Vec<JoystickDisplayInfo>,
    }

    #[derive(Debug)]
    pub struct JoystickState {
        pub button_states: Vec<bool>,
    }

    #[derive(Debug)]
    pub struct JoystickDisplayInfo {
        pub name: String,
    }

    impl RawInputManager {
        pub fn new() -> Result<Self, &'static str> {
            panic!()
        }

        pub fn register_devices(&self, _: DeviceType) {
            panic!()
        }

        pub fn get_device_list(&self) -> DevicesDisplayInfo {
            panic!()
        }

        pub fn filter_devices(&self, _: Vec<String>) {
            panic!()
        }

        pub fn get_joystick_state(&self, _: usize) -> Option<JoystickState> {
            panic!()
        }

        pub fn get_events(&mut self) -> std::sync::mpsc::TryIter<RawEvent> {
            panic!()
        }
    }
}
