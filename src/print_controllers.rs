use {
    super::settings::Settings,
    trainsim_bindings::{BindError, TrainSimBindings},
};

pub async fn main(settings: Settings) -> Result<(), BindError> {
    let bindings = TrainSimBindings::new(settings.raildriver_dll_path().as_path())?;
    println!("{}", bindings.controllers().join("\n"));
    Ok(())
}
