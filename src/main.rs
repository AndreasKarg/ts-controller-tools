#![feature(adt_const_params)]

//! A collection of more or less useful tools for controlling or otherwise interacting with
//! Dovetail Games' Train Simulator (not Train Sim World) series.
//!
//! # Features
//! ## Cruise Control
//!
//! Do you wish you could just sit in the passenger view of your train and enjoy the world
//! go past while someone else does the driving for you? Then you'll like
//! `cruise-control`.
//!
//! All you have to do is set up the train ready for departure, open your browser on any device in
//! your local network (e.g. your phone) and tell it about the current and upcoming speed limits
//! and stops.
//!
//! ## "Sven's Controller" (experimental)
//!
//! Support for a particular type of controller designed for the Japanese BVE simulator.
//! Over time, this is intended to grow into generic support for joysticks and similar controllers.
//!
//! # Installation
//! ## Windows Binary Release
//!
//! Currently, binary releases are only provided for the 64-bit version of Train Simulator.
//! These will not work with 32-bit Train Simulator.
//!
//! - Download the most recent release from [GitLab](https://gitlab.com/AndreasKarg/ts-controller-tools/-/releases).
//! - Extract the zip file to a new folder of your choice.
//! - Create a new folder `<Your Documents folder>\ts-controller-tools`
//! - Copy `config-template.toml` from the zip file into the new folder and rename it to `config.toml`
//! - Open `config.toml` in your favourite text editor (Notepad will do) and change the first line
//!   to your Train Simulator installation path.
//! - Open a command-line terminal in the folder you extracted `ts-controller-tools` into.
//! - Run `ts-controller-tools.exe print-controllers`.
//! - If all went well, it should now print
//!   `Error: Train Simulator is not connected. Have you started a scenario yet?` and quit - or,
//!   if you are already running a Train Simulator scenario, print a long list of things.
//!
//! ## From Source
//!
//! ---
//! **NOTE**
//!
//! `ts-controller-tools` currently requires a Nightly build of the Rust toolchain.
//!
//! ---
//! - Clone the repository
//! - Copy `config-template.toml` from the zip file into the new folder and rename it to `config.toml`
//! - Open `config.toml` in your favourite text editor (Notepad will do) and change the first line
//!   to your Train Simulator installation path.
//! - Run `cargo run -- print-controllers` from the repo root folder.
//! - If all went well, it should now print
//!   `Error: Train Simulator is not connected. Have you started a scenario yet?` and quit - or,
//!   if you are already running a Train Simulator scenario, print a long list of things.
//!
//! # Usage
//!
//! - Open a command-line terminal in the installation folder.
//! - Run `ts-controller-tools.exe <command> [parameters]` to start the tool or
//!   `ts-controller-tools.exe --help` for more information.

mod cruise_control;

mod print_controllers;
mod raildriver;
// mod setpoint_interpolator;
mod settings;
mod svens_controller;
mod sync_helpers;
mod eifel_control_desk;

#[cfg(test)]
#[macro_use]
extern crate more_asserts;

#[cfg(test)]
#[macro_use]
extern crate rstest;

#[macro_use]
extern crate derive_new;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate thiserror;

use uom::lib::time::Duration;
use {anyhow::Result, clap::Parser, settings::Settings};

const CRATE_NAME: &str = env!("CARGO_PKG_NAME");
const CRATE_VERSION: &str = env!("CARGO_PKG_VERSION");
const CRATE_AUTHOR: &str = env!("CARGO_PKG_AUTHORS");

/// A collection of more or less useful tools for controlling or otherwise interacting with
/// Dovetail Games' Train Simulator (not Train Sim World) series.
#[derive(Parser)]
#[clap(version = CRATE_VERSION, author = CRATE_AUTHOR)]
struct Opts {
    #[clap(subcommand)]
    subcommand: Subcommand,
}

#[derive(Parser)]
enum Subcommand {
    #[clap(version = CRATE_VERSION, author = CRATE_AUTHOR)]
    /// Drives the train so you don't have to. Sort-of.
    CruiseControl {
        /// The name of the vehicle profile you want to use, as listed in your config file.
        vehicle_profile: String,
    },
    #[clap(version = CRATE_VERSION, author = svens_controller::SUBCOMMAND_AUTHOR)]
    /// Lets you drive your train using a particular Japanese controller designed for BVE.
    SvensController {
        /// The name of the vehicle profile you want to use, as listed in your config file.
        vehicle_profile: String,

        /// The interval (in milliseconds) at which the controller is polled.
        #[clap(default_value = "50")]
        poll_interval_ms: u64,

        /// The number of sample intervals (see poll-interval-ms) for which the controller has to
        /// remain in a particular position before it gets recognized.
        #[clap(default_value = "3")]
        debounce_threshold: u16,
    },
    #[clap(version = CRATE_VERSION)]
    /// Lets you drive your train using a certain control desk in a certain place.
    EifelControlDesk {
        /// The name of the vehicle profile you want to use, as listed in your config file.
        vehicle_profile: String,

        /// The interval (in milliseconds) at which the controller is polled.
        #[clap(default_value = "50", long, short)]
        poll_interval_ms: u64,

        /// The number of sample intervals (see poll-interval-ms) for which the controller has to
        /// remain in a particular position before it gets recognized.
        #[clap(default_value = "3", long, short)]
        debounce_threshold: u16,
    },
    #[clap(version = CRATE_VERSION, author = CRATE_AUTHOR)]
    /// Lists all "controllers" (throttle lever, speedometer, ...) in the currently active vehicle.
    PrintControllers,
}

#[tokio::main]
async fn main() {
    if let Err(err) = run().await {
        eprintln!("Error: {:?}", err);
        std::process::exit(1);
    }
}

async fn run() -> Result<()> /*Result<(), Box<dyn std::error::Error>>*/ {
    let opts: Opts = Opts::parse();

    let settings = Settings::load()?;

    match opts.subcommand {
        Subcommand::CruiseControl { vehicle_profile } => {
            cruise_control::main(settings, vehicle_profile).await?
        }
        Subcommand::SvensController {
            vehicle_profile,
            poll_interval_ms,
            debounce_threshold,
        } => {
            svens_controller::main(
                settings,
                vehicle_profile,
                Duration::from_micros(poll_interval_ms),
                debounce_threshold,
            )
            .await?
        },
        Subcommand::EifelControlDesk {
            vehicle_profile,
            poll_interval_ms,
            debounce_threshold,
        } => {
            eifel_control_desk::main(
                settings,
                vehicle_profile,
                Duration::from_micros(poll_interval_ms),
                debounce_threshold,
            )
                .await?
        },
        Subcommand::PrintControllers => print_controllers::main(settings).await?,
    }

    Ok(())
}
